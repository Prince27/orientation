**What is Docker?**

_An open-source project that automates the deployment of software applications inside containers by providing an additional layer of abstraction and automation of OS-level virtualization on Linux_.

**Why Docker?**

_For developers, there arises a situation where they need to work on different projects. And for each different project they might need a specific operating system, or a specific version of operating system. This becomes a tedious job to do. This problem can be solved using virtual machines, but this also requires exact enviornment to run an application. Here comes the concept of Docker. The developer can work on the application and save it as docker image and create a docker container. This container has everything which is required to run an application alongwith the enviornment. This docker is then stored to repository and then can be used by other developers. So docker is used by many developers._

<img src="extras/docker.png">

**Docker Terminologies**

* _Docker_
* _Docker Image_
* _Container_
* _Docker Hub_

_Docker is a program for developers to develop, and run applications with containers.A Docker image is contains everything needed to run an applications as a container. This includes:code,runtime,libraries,environment variables,configuration files.A Docker container is a running Docker image.From one image you can create multiple containers.Docker Hub is like GitHub but for docker images and containers_.

<img src="extras/gw2.jpg">

**Docker Components**

* Docker Engine:
The place where containers and runs the programs.
* Docker Client:
It is the end user which provides the demands of the docker file as commands.
* Docker daemon:
This place checks the client request and communicate with docker components such as image, container, to perform the process.
* Docker Registry:
A place docker images are stored. DockerHub is such a public registry.


<img src="extras/dc.png">





**Operations on Dockers**

* Download the docker.
``` docker pull snehabhapkar/trydock```
* Run the docker image with this command
``` docker run -ti snehabhapkar/trydock /bin/bash```
* Copy file inside the docker container
``` docker cp hello.py e0b72ff850f8:/```
* All write a script for installing dependencies - requirements.sh
``` apt update```
``` apt install python3```
********
## **Docker Installation**
Docker is easy to install.<br>
It runs on:
- A variety of Linux distributions.
- OS X via a virtual machine.
- Microsoft Windows via a virtual machine.<br>

 **Install docker on ubuntu 16.04 LTS**<br>

 $ sudo apt-get update
$ sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common
$ curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
$ sudo apt-key fingerprint 0EBFCD88
$ sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable nightly test"<br>
$ sudo apt-get update<br>
$ sudo apt-get install docker-ce docker-ce-cli containerd.io<br>

// Check if docker is successfully installed in your system <br>
$ sudo docker run hello-world 

## **Docker Basics Commands**
- docker run – Runs a command in a new container.
- docker start – Starts one or more stopped containers
- docker stop – Stops one or more running containers
- docker build – Builds an image form a Docker file
- docker pull – Pulls an image or a repository from a registry
- docker push – Pushes an image or a repository to a registry
- docker export – Exports a container’s filesystem as a tar archive
- docker exec – Runs a command in a run-time container
- docker search – Searches the Docker Hub for images
- docker attach – Attaches to a running container
- docker commit – Creates a new image from a container’s changes


