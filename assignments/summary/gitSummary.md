
<img src="extras/git.png">



**GIT**

_Git is a version control (VCS) system for tracking changes to projects_.
_Version control systems are also called revision control systems or source code management (SCM) systems_.

_Git is a distributed version-control system for tracking changes in source code during software development. It is designed for coordinating work among programmers, but it can be used to track changes in any set of files. Its goals include speed, data integrity, and support for distributed, non-linear workflows_.


<img src="extras/vcs.png">

*Using Git*

**What’s the point of Git?**

_Suppose you’re working on a group assignment with some friends, and everyone has their own brilliant ideas. Consider two ways to combine these ideas together:_

* _Person A writes his/her/their ideas on a single sheet of paper and passes it on to Person B, who writes his/her/their ideas on the paper and passes it on to Person C, etc._
* _Everyone writes their ideas down on separate pieces of paper, and then consolidates them into a single sheet afterward._


**Using Git**

_Before starting to use Git you should first tell Git who you are so that you can be identified when you make contributions to projects. This can be done by specifying your name and email address_.

```
git config --global user.name "Your Name Comes Here"
git config --global user.email your_email@yourdomain.example.com 
```git config --global user.name "Your Name Comes Here"
   git config --global user.email your_email@yourdomain.example.com 
``` 

*Starting a Project with Git*
**Starting a Project with Git**

_You can create a directory for your new project using the mkdir command_.

```
mkdir myproject
```mkdir myproject
```
_or you can work with a a project directory that already exists. You can enter that directory with_

```
cd myproject
```cd myproject
```


*Summary of Essential Git Commands*
**Summary of Essential Git Commands**

``` git status: _check status and see what has changed_
    git add: _add a changed file or a new file to be committed_
    git pull: _pull changes from a remote repoository_
```

*Git workflow*
**Git workflow**
<img src="extras/gw.jpeg">

**Basic Git commands**<br>
To use Git, developers use specific commands to copy, create, change, and combine code. These commands can be executed directly from the command line or by using an application like GitHub Desktop or Git Kraken. Here are some common commands for using Git:

**git init** initializes a brand new Git repository and begins tracking an existing directory. It adds a hidden subfolder within the existing directory that houses the internal data structure required for version control.

**git clone** creates a local copy of a project that already exists remotely. The clone includes all the project’s files, history, and branches.

**git add** stages a change. Git tracks changes to a developer’s codebase, but it’s necessary to stage and take a snapshot of the changes to include them in the project’s history. This command performs staging, the first part of that two-step process. Any changes that are staged will become a part of the next snapshot and a part of the project’s history. Staging and committing separately gives developers complete control over the history of their project without changing how they code and work.

**git commit** saves the snapshot to the project history and completes the change-tracking process. In short, a commit functions like taking a photo. Anything that’s been staged with git add will become a part of the snapshot with git commit.

**git status** shows the status of changes as untracked, modified, or staged.

**git branch** shows the branches being worked on locally.

**git merge** merges lines of development together. This command is typically used to combine changes made on two distinct branches. For example, a developer would merge when they want to combine changes from a feature branch into the main branch for deployment.

**git pull** updates the local line of development with updates from its remote counterpart. Developers use this command if a teammate has made commits to a branch on a remote, and they would like to reflect those changes in their local environment.

**git push** updates the remote repository with any commits made locally to a branch.

## **Git workflows**

<img src="extras/work.png">

